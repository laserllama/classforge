import pytest
import base64

from classforge import Class, StrictClass, Field

# =======================================================================

class DefaultDemo(Class):

    x = Field(default=3.14)
    y = Field(default=3000)

    def __init__(self, **kwargs):
        super(DefaultDemo, self).__init__(**kwargs)

def test_default():

    obj = DefaultDemo(y=-2)
    assert obj.x == 3.14
    obj.x = 3
    assert obj.x == 3

    assert obj.y == -2

    obj2 = DefaultDemo(x=5, y=10)
    assert obj2.x == 5
    assert obj2.y == 10

    obj3 = DefaultDemo(y=15)
    assert obj3.x == 3.14
    assert obj3.y == 15

def test_default_explain():

    obj1 = DefaultDemo(x=2)
    explain = obj1.explain()
    assert explain['x']['default'] == 3.14
    assert explain['x']['value'] == 2
    assert explain['x']['required'] == False
    assert explain['x']['_class'] == 'Field'

# =======================================================================

class SomeBasicClass(object):

    def __init__(self, foo):

        self.foo = foo

class TypesDemo(Class):

    x = Field(type=int)
    y = Field(type=str)
    z = Field(type=SomeBasicClass)

def test_basic():

    obj = TypesDemo()
    obj.x = 2
    obj.y = "Hello"
    obj.z = SomeBasicClass(1234)
    assert obj.x == 2
    assert obj.y == "Hello"
    assert obj.z.foo == 1234

    with pytest.raises(ValueError) as e_info:
        obj.x = "Hello"
    with pytest.raises(ValueError) as e_info:
        obj.y = 3
    with pytest.raises(ValueError) as e_info:
        obj.x = SomeBasicClass(1234)
    with pytest.raises(ValueError) as e_info:
        obj.z = 42

# =======================================================================

class RequiredDemo(Class):

    a = Field(type=int, default=42)
    b = Field(type=int, required=True)

def test_required():

    obj1 = RequiredDemo(a=1, b=2)
    obj2 = RequiredDemo(b=2)
    with pytest.raises(AttributeError) as e_info:
        obj3 = RequiredDemo()
    with pytest.raises(AttributeError) as e_info:
        obj4 = RequiredDemo(a=2)

# =======================================================================

class NullableDemo(Class):

    a = Field(type=int, nullable=False)
    b = Field(type=int)

def test_nullable():

    obj = NullableDemo()
    obj.a = 25
    obj.b = None
    assert obj.a == 25
    assert obj.b == None

    with pytest.raises(ValueError) as e_info:
        obj.a = None

# =======================================================================

class MutableDemo(Class):

    a = Field(type=int)
    b = Field(type=int, default=10, mutable=False)

def test_mutable():

    obj = MutableDemo()
    obj.a = 5
    obj.a = 6
    with pytest.raises(ValueError) as e_info:
        obj.b = 11

# =======================================================================

PETS = [ 'dog', 'cat', 'chicken' ]

class ChoicesDemo(Class):
    pet = Field(type=str, choices=PETS)

def test_choices():

    c = ChoicesDemo()
    c.pet = 'dog'
    c.pet = 'cat'
    c.pet = 'chicken'
    with pytest.raises(ValueError) as e_info:
        c.pet = 'velociraptor'

# =======================================================================

class AccessorDemo(Class):

    a = Field(type=int, accessor='renamed_get_a')
    b = Field(type=int)

    def renamed_get_a(self, value):
        return value * 10

    def get_b(self, actual_value):
        return actual_value * 100


def test_accessor_demo():

    obj = AccessorDemo()
    obj.a = 50
    obj.b = 60
    assert obj.a == 500
    assert obj.b == 6000

# =======================================================================

class MutatorDemo(Class):

    a = Field(type=int, mutator='renamed_set_a')
    b = Field(type=int)
    c = Field(type=int)

    def renamed_set_a(self, input_value):
        return input_value * 10

    def set_b(self, input_value):
        return input_value * 100

    def set_c(self, input_value):
        if input_value < 5000:
            raise ValueError("too low")
        return input_value

def test_mutator_demo():

    obj = MutatorDemo()
    obj.a = 50
    obj.b = 60
    obj.c = 9999

    assert obj.a == 500
    assert obj.b == 6000
    assert obj.c == 9999

    with pytest.raises(ValueError) as e_info:
        obj.c = 4000
    assert obj.c == 9999

# =======================================================================

class SerializationDemo(Class):

    f = Field(type=int, default=10)
    g = Field(type=int, default=20)
    h = Field(type=int, default=30)

class NestedSerializationDemo(Class):

    c = Field(type=SerializationDemo)
    d = Field(type=int, default=40)
    e = Field(type=int, default=50)

class DeeperSerializationDemo(Class):

    a = Field(type=NestedSerializationDemo)
    b = Field(type=int, default=60)

def test_serialization():

    obj = SerializationDemo(f=5)

    serialized = obj.to_dict()
    assert serialized['f'] == 5
    assert serialized['g'] == 20
    assert serialized['h'] == 30

    obj2 = SerializationDemo.from_dict(serialized)
    assert obj2.f == 5
    assert obj2.g == 20
    assert obj2.h == 30

def test_nested_object_serialzation():

    obj1 = DeeperSerializationDemo(
        a = NestedSerializationDemo(
            c = SerializationDemo(
                g  = 80
            ),
            d = 90
        ),
        b = 70,
    )


    data = obj1.to_dict()
    assert data == {'a': {'c': {'f': 10, 'g': 80, 'h': 30}, 'd': 90, 'e': 50}, 'b': 70}

    obj2 = DeeperSerializationDemo.from_dict(data)
    assert obj2.b == 70
    assert obj2.a.c.f == 10

    yaml = obj2.to_yaml()
    obj3 = DeeperSerializationDemo.from_yaml(yaml)
    assert obj3.a.c.f == 10

    json = obj2.to_json(sort_keys=True, indent=4)
    obj4 = DeeperSerializationDemo.from_json(json)
    assert obj4.a.c.f == 10


# =======================================================================

ACME_CLOUD='acme_cloud'
STARK_INDUSTRIES='stark_industries'

class AdvancedSerializationDemo(Class):

    basic_example    = Field(type=int, remap='basicExample', required=True)

    hidden           = Field(type=int, hidden=True)

    count            = Field(type=int, remap=dict(acme_cloud='productAmount'))
    instance_size    = Field(type=int, remap=dict(acme_cloud='instanceSize', stark_industries='instanceCapacity'))
    sometimes_hidden = Field(type=int, remap=dict(acme_cloud=None))

def test_remap_and_hidden():

    obj1 = AdvancedSerializationDemo(basic_example=0, count=1, instance_size=2,sometimes_hidden=4)
    data = obj1.to_dict()
    assert data == dict(basicExample=0, count=1, instance_size=2, sometimes_hidden=4)

    acme_data = obj1.to_dict(ACME_CLOUD)
    assert acme_data == dict(basicExample=0, productAmount=1, instanceSize=2)

    stark_data = obj1.to_dict(STARK_INDUSTRIES)
    assert stark_data == dict(basicExample=0, count=1, instanceCapacity=2, sometimes_hidden=4)

    acme_obj = AdvancedSerializationDemo.from_dict(acme_data, ACME_CLOUD)
    assert acme_obj.basic_example    == obj1.basic_example
    assert acme_obj.hidden           == None
    assert acme_obj.count            == 1
    assert acme_obj.instance_size    == 2
    assert acme_obj.sometimes_hidden == None

    stark_obj = AdvancedSerializationDemo.from_dict(stark_data, STARK_INDUSTRIES)
    assert stark_obj.basic_example    == obj1.basic_example
    assert stark_obj.hidden           == None
    assert stark_obj.count            == 1
    assert stark_obj.instance_size    == 2
    assert stark_obj.sometimes_hidden == 4

# =======================================================================

class CustomSerializationDemo(Class):

    foo    = Field(type=str, nullable=False)
    bar    = Field(type=str, nullable=False, encode='renamed_encode_bar', decode='renamed_decode_bar')

    def encode_foo(self, value, remap_name=None):
        print("calling custom encode with %s" % value)
        return base64.b64encode(bytes(value, 'utf-8'))

    @classmethod
    def decode_foo(cls, value, remap_name=None):
        print("calling custom2 encode with %s" % value)
        return str(base64.b64decode(value), 'ascii')

    def renamed_encode_bar(self, value, remap_name=None):
        return base64.b64encode(bytes(value, 'utf-8'))

    def renamed_decode_bar(self, value, remap_name=None):
        return str(base64.b64decode(value), 'ascii')

def test_custom_serialization():

    obj = CustomSerializationDemo()
    obj.foo = "Happy Birthday!"
    obj.bar = "Happy Birthday!"

    serialized = obj.to_dict()
    assert serialized['foo'] == b'SGFwcHkgQmlydGhkYXkh'
    assert serialized['bar'] == b'SGFwcHkgQmlydGhkYXkh'

    obj2 = CustomSerializationDemo.from_dict(serialized)
    assert obj2.foo == "Happy Birthday!"
    assert obj2.bar == "Happy Birthday!"

# ============================================

class Base64DemoField(Field):

    def __init__(self, **kwargs):
        kwargs['type'] = str
        super(Base64DemoField, self).__init__(**kwargs)

    def field_encode(self, value, remap_name=None):
        return base64.b64encode(bytes(value, 'utf-8'))

    def field_decode(self, value, remap_name=None):
        return str(base64.b64decode(value), 'ascii')

class SubFieldTwo(Field):

    def field_access(self, value):
        return value * 100

class SubFieldThree(Field):

    def field_mutate(self, value):
        return value * 100

class ClassWithSubField(Class):

    foo = Base64DemoField(nullable=False)
    bar = SubFieldTwo(type=int)
    baz = SubFieldThree(type=int)

def test_subfield():

    obj = ClassWithSubField(bar=5)
    obj.foo = "Happy Birthday!"
    obj.baz = 10
    serialized = obj.to_dict()

    assert serialized['foo'] == b'SGFwcHkgQmlydGhkYXkh'
    obj2 = ClassWithSubField.from_dict(serialized)
    assert obj.foo == "Happy Birthday!"

    assert obj.bar == 500
    assert obj.baz == 1000

# ============================================

class DiamondBase(Class):

    a = Field(type=int, default=1)
    b = Field(type=int, default=2)
    c = Field(type=int, default=3)
    d = Field(type=int, default=4)
    e = Field(type=int, default=5)
    f = Field(type=int, default=6)
    g = Field(type=str, default="Hi world")

class LeftSide(DiamondBase):

    e = Field(type=int, default=7)
    f = Field(type=int, default=8)

    def __init__(self, **kwargs):
        # make sure constructors don't mess anything up
        super(LeftSide, self).__init__(**kwargs)

class RightSide(DiamondBase):

    e = Field(type=int, default=9)
    f = Field(type=int, default=10)

class Child(LeftSide, RightSide):

    b = Field(type=int, default=12)
    c = Field(type=str, default='hello')
    f = Field(type=int, default=11)

def test_inheritance():

    obj1 = DiamondBase()
    assert obj1.a == 1
    assert obj1.b == 2
    assert obj1.c == 3
    assert obj1.d == 4
    assert obj1.e == 5
    assert obj1.f == 6
    assert obj1.g == "Hi world"
    obj1.a == -99

    obj2 = LeftSide(a=-5)
    assert obj2.a == -5
    assert obj2.b == 2
    assert obj2.e == 7
    assert obj2.f == 8

    obj3 = RightSide(f=-1000)
    assert obj3.a == 1
    assert obj3.e == 9
    assert obj3.f == -1000

    obj4 = Child()
    assert obj4.b == 12
    assert obj4.c == 'hello'
    assert obj4.f == 11
    assert obj4.e in [ 7, 9 ] # heh
    assert obj4.a == 1

# =======================================================================

class JunkInputDemo(Class):

    x = Field(type=int, default=15)

def test_junk_input():

    obj1 = JunkInputDemo()
    obj1.x = 5
    with pytest.raises(AttributeError) as e_info:
        obj1.z = 24

# ========================================================================

ON_INIT_TEST = []

class A(Class):

    x = Field(type=int, default=5)

    def on_init(self):
        ON_INIT_TEST.append('A')

class B(A):

    y = Field(type=int)

    def on_init(self):
        ON_INIT_TEST.append('B')


class C(B):

    def on_init(self):
        ON_INIT_TEST.append('C')


def test_on_init():

    obj = C(y=20)
    assert obj.y == 20
    assert obj.x == 5
    assert ON_INIT_TEST == [ 'A', 'B', 'C' ]

# ========================================================================

class StrictDemo(StrictClass):

    x = Field(type=int)
    y = Field(type=int, default=30, mutable=True)
    z = Field(type=int, default=20)

class BrokenStrictDemo(StrictClass):

    x = Field(default=20)

def test_strict():

    obj = StrictDemo(x=1, y=2, z=3)

    # test that fields are immutable by default
    with pytest.raises(ValueError) as e_info:
        obj.x = 3

    # test that no default implies a required field
    with pytest.raises(AttributeError) as e_info:
        obj = StrictDemo(y=5, z=6)

    # test that type is required
    with pytest.raises(AttributeError) as e_info:
        obj = BrokenStrictDemo(x=50)



