ClassForge
==========

A new object system for python - see http://classforge.io/ for docs

You can follow @classforge on twitter for updates.

Installation
============

pip3 install classforge

Author
======

(C) Michael DeHaan, 2020

MIT Licensed
